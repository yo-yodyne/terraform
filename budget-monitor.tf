resource "oci_budget_budget" "yo-yodyne_budget" {
    amount = var.budget_amount
    compartment_id = var.tenancy_ocid
    reset_period = "MONTHLY"
    description = "Yo-Yodyne Budget "
    display_name = "yo-yodyne_budget"
    target_type  = "COMPARTMENT"
    targets      = [var.tenancy_ocid]
}

resource "oci_budget_alert_rule" "yo-yodyne_alert_rule" {
    #Required
    budget_id = oci_budget_budget.yo-yodyne_budget.id
    threshold = 1
    threshold_type = "PERCENTAGE"
    type = "FORECAST"
    recipients = var.alert_rule_recipients
    message = "Budget for Yo-Yodyne resources in Oracle Cloud Infrastructure (OCI) has been exceeded."
}
