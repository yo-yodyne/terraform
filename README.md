# Oracle Cloud Infrastructure (OCI) setup for Yo-Yodyne.com
This Terraform code builds out the infrastructure to run Yo-Yodyne.com on OCI.

Yo-Yodyne runs on the OCI "Always Free Tier", so instance sizes are limited to what is currently supported for free. Running a cost analysis post deployment would help ensure that additional charges are not incurred. An OCI Budget and Alarm is configured to facilitate cost monitoring.  

# Usage
Edit variables required by the "terraform.tfvars.tmpl" file.

The deploy stage only runs on the main branch. 

# Terraform .tf files overview
- availability-domains.tf:
  - Creates a simple availability domain data resource.  

- budget-monitor.tf:
  - Creates a budget with a $1 threshold with alert that emails the address  
  defined by the `$OCI_ALERT_RULE_RECIPIENTS` variable.

- compute.tf:
  - Creates compute resource(s).

- network.tf:
  - Creates networking infrastructure subnet (192.168.0.0/24) for compute  
  resources, an Internet Gateway, and ingress rules that allow anyone (0.0.0.0) 
  to ingress over ports 22 (ssh), 80 (HTTP), and 443 (HTTPS).

- outputs.tf:
  - Creates availability domain data outputs 

- provider.tf:
  - Uses the variables in terraform.tfvars, rendered from terraform.tfvars.  
  tmpl, to configure the OCI provider

- variables.tf:
  - Defines input variables
